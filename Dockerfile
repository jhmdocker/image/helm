FROM registry.gitlab.com/jhmdocker/image/ubuntu:24.04

ARG HELM_VERSION=ND
ENV HELM_VERSION=$HELM_VERSION

RUN apt-get update && apt-get install -y wget

RUN echo HELM_VERSION=$HELM_VERSION && \
    wget -qO helm.tgz "https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz" && \
    cd /usr/local/bin/ && \
    tar -xzf /helm.tgz && \
    rm -fr /helm.tgz && \
    ls -lh && \
    cp linux-amd64/helm . && \
    chmod +x helm && \
    rm -fr linux-amd64 && \
    echo END
