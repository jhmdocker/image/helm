VERSION=v3.16.4
IMG=registry.gitlab.com/jhmdocker/image/helm

build:
	docker build $(BUILD_OPTS) -t $(IMG) \
		--build-arg HELM_VERSION=$(VERSION) \
		.

push: build
	docker tag $(IMG) $(IMG):$(VERSION)
	docker push $(IMG)
	docker push $(IMG):$(VERSION)
